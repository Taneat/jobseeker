const express = require('express')
const router = express.Router()
const jobseekerController = require('../controller/JobseekerController')

router.get('/', jobseekerController.getUsers)
router.get('/:id', jobseekerController.getUser)
router.post('/', jobseekerController.addUser)
router.put('/', jobseekerController.updateUser)
router.delete('/:id', jobseekerController.deleteUser)

module.exports = router
